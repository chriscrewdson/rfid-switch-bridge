require 'bundler/setup'
require 'sinatra'
require 'oauth2'
require 'json'
require 'net/http'
require 'uri'
require 'redis'
require 'base64'
require 'cgi'
require 'openssl'

# Our client ID and secret, used to get the access token
CLIENT_ID = ENV['ST_CLIENT_ID']
CLIENT_SECRET = ENV['ST_CLIENT_SECRET']

# We'll store the access token in the session
use Rack::Session::Pool, :cookie_only => false

# This is the URI that will be called with our access
# code after we authenticate with our SmartThings account
redirect_uri = 'https://rfid-switch-bridge.herokuapp.com/oauth/callback'

# This is the URI we will use to get the endpoints once we've received our token
endpoints_uri = 'https://graph.api.smartthings.com/api/smartapps/endpoints'

redis = Redis.new(url: ENV["REDIS_URL"])

options = {
  site: 'https://graph.api.smartthings.com',
  authorize_url: '/oauth/authorize',
  token_url: '/oauth/token'
}

# use the OAuth2 module to handle OAuth flow
client = OAuth2::Client.new(CLIENT_ID, CLIENT_SECRET, options)

def authenticated?
  session[:access_token]
end


# handle requests to the application root
get '/' do
  %(<a href="/authorize">Connect with SmartThings</a>)
end

# handle requests to /authorize URL
get '/authorize' do
  # Use the OAuth2 module to get the authorize URL.
  # After we authenticate with SmartThings, we will be redirected to the
  # redirect_uri, including our access code used to get the token
  url = client.auth_code.authorize_url(redirect_uri: redirect_uri, scope: 'app')
  redirect url
end

# handle requests to /oauth/callback URL. We
# will tell SmartThings to call this URL with our
# authorization code once we've authenticated.
get '/oauth/callback' do
  # The callback is called with a "code" URL parameter
  # This is the code we can use to get our access token
  code = params[:code]

  puts 'headers: ' + headers.to_hash.inspect

  # Use the code to get the token.
  response = client.auth_code.get_token(code, redirect_uri: redirect_uri, scope: 'app')

  # now that we have the access token... 
  # store it in the session for admin use
  session[:access_token] = response.token
  # and in redis for other calls of this app (/scan) to use
  redis.set(:access_token, response.token)

  # debug - inspect the running console for the
  # expires in (seconds from now), and the expires at (in epoch time)
  puts 'TOKEN EXPIRES IN ' + response.expires_in.to_s
  puts 'TOKEN EXPIRES AT ' + response.expires_at.to_s
  redirect '/info'
end

# handle requests to the /info URL. This is where
# we will make requests to get information about the configured
# switch.
get '/info' do
  # If we get to this URL without having gotten the access token
  # redirect back to root to go through authorization
  if !authenticated?
    redirect '/'
  end

  token = session[:access_token]

  # make a request to the SmartThings endpoint URI, using the token,
  # to get our endpoints
  url = URI.parse(endpoints_uri)
  req = Net::HTTP::Get.new(url.request_uri)

  # we set a HTTP header of "Authorization: Bearer <API Token>"
  req['Authorization'] = 'Bearer ' + token

  http = Net::HTTP.new(url.host, url.port)
  http.use_ssl = (url.scheme == "https")

  response = http.request(req)
  json = JSON.parse(response.body)

  # debug statement
  puts json

  # get the endpoint from the JSON:
  uri = json[0]['uri']

  # now we can build a URL to our WebServices SmartApp
  # we will make a GET request to get information about the switch
  switchUrl = uri + '/switches'

  # debug
  puts "SWITCH ENDPOINT: " + switchUrl

  getSwitchURL = URI.parse(switchUrl)
  getSwitchReq = Net::HTTP::Get.new(getSwitchURL.request_uri)
  getSwitchReq['Authorization'] = 'Bearer ' + token

  getSwitchHttp = Net::HTTP.new(getSwitchURL.host, getSwitchURL.port)
  getSwitchHttp.use_ssl = true

  switchStatus = getSwitchHttp.request(getSwitchReq)

  '<h3>Response Code</h3>' + switchStatus.code +
    '<br/><h3>Response Body</h3>' + switchStatus.body +
    '<br/><h3>Switch State</h3>' + redis.get(:switch_state).to_s +
    '<br/><h3>Last scanned device ID</h3>' + redis.get(:last_scanned_device_id).to_s +
    '<br/><h3>Last scanned tag UID</h3>' + redis.get(:last_scanned_tag_uid).to_s +
    '<br/><h3>Counter</h3>' + redis.get(:counter).to_s +
    '<br/><h3>Request Hash</h3>' + redis.get(:hash).to_s +
    '<br/><h3>Computed Hash</h3>' + redis.get(:computed_hash).to_s
end

post '/scan' do
  # Swap state
  swappedState = redis.get(:switch_state) == 'on' ? 'off' : 'on'
  redis.set(:switch_state, swappedState)
  
  lastCounter = redis.get(:counter)
  
  # example scan request body https://bitbucket.org/chriscrewdson/nfc-scanner
  # {
  #   "device_id": "CONFIG-DEVICE-ID",
  #   "tag_uid": "THE-TAGS-UUID",
  #   "counter": 3,
  #   "hash": "7cac4442e6caf834ce20a359ff7ee9ff34525691"
  # }

  request.body.rewind
  requestBody = JSON.parse(request.body.read)
  
  requestDeviceId = requestBody['device_id'].to_s
  requestTagUid = requestBody['tag_uid'].to_s
  requestCounter = requestBody['counter'].to_s
  requestHash = requestBody['hash'].to_s

  puts 'DEVICE ID:' + requestDeviceId
  puts 'TAG UID:' + requestTagUid
  puts 'COUNTER: ' + requestCounter
  puts 'HASH: ' + requestHash

  redis.set(:last_scanned_device_id, requestDeviceId)
  redis.set(:last_scanned_tag_uid, requestTagUid)

  redis.set(:counter, requestCounter)
  redis.set(:hash, requestHash)

  # todo: set these from an /info put
  approvedDeviceId = ENV['APPROVED_DEVICE_ID']
  approvedTagUid = ENV['APPROVED_TAG_UID']

  expectedCounter = (lastCounter.to_i + 1).to_s

  computedHash = hmac_sha1(
    approvedDeviceId + approvedTagUid + expectedCounter,
    ENV['DEVICE_SECRET']
  )

  redis.set(:computed_hash, computedHash)

  if (
    requestDeviceId == approvedDeviceId &&
    requestTagUid == approvedTagUid &&
    requestCounter == expectedCounter &&
    requestHash == computedHash
  ) then
    token = redis.get(:access_token)

    if (token) then
      # make a request to the SmartThings endpoint URI, using the token,
      # to get our endpoints
      url = URI.parse(endpoints_uri)
      req = Net::HTTP::Get.new(url.request_uri)

      # we set a HTTP header of "Authorization: Bearer <API Token>"
      req['Authorization'] = 'Bearer ' + token.to_s

      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = (url.scheme == "https")

      response = http.request(req)
      json = JSON.parse(response.body)

      # get the endpoint from the JSON:
      uri = json[0]['uri']

      # now we can build a URL to our WebServices SmartApp
      # we will make a POST request to set switch information
      switchStateUrl = uri + '/switches/' + swappedState

      # debug
      puts "SWITCH STATE ENDPOINT: " + switchStateUrl

      switchStateUri = URI.parse(switchStateUrl)
      switchStateReq = Net::HTTP::Put.new(switchStateUri.request_uri)
      switchStateReq['Authorization'] = 'Bearer ' + token

      switchStateHttp = Net::HTTP.new(switchStateUri.host, switchStateUri.port)
      switchStateHttp.use_ssl = true

      switchStateResponse = switchStateHttp.request(switchStateReq)

      content_type :json
      {
        :r => 0,
        :g => 250,
        :b => 0,
        :delay => 2000,
        :internalResponseCode => switchStateResponse.code,
        :internalState => swappedState.to_s
      }.to_json
    else
      content_type :json
      {
        :r => 255,
        :g => 0,
        :b => 0,
        :delay => 2000,
        :internalState => swappedState.to_s,
        :internalError => 'Could not get token'
      }.to_json
    end
  else
    content_type :json
    {
      :r => 255,
      :g => 0,
      :b => 0,
      :delay => 2000,
      :internalState => swappedState.to_s,
      :internalError => 'Unauthorized'
    }.to_json
  end
end

def hmac_sha1(data, secret)
  hmac = OpenSSL::HMAC.hexdigest(
    OpenSSL::Digest.new('sha1'),
    secret.encode("ASCII"),
    data.encode("ASCII")
  )
  return hmac
end
